import pytest


@pytest.mark.asyncio
async def test_load_defaults(mock_hub):
    # Hard coded grains
    assert mock_hub.grains.GRAINS.os_family == "BSD"
    assert mock_hub.grains.GRAINS.ps == "ps auxwww"
